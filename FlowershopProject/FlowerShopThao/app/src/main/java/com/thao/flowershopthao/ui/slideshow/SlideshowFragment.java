package com.thao.flowershopthao.ui.slideshow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.thao.flowershopthao.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;
public class SlideshowFragment extends Fragment {

//    private SlideshowViewModel slideshowViewModel;


        private ViewPager mViewPager;

        private List<SlideImage> mSlideImages;
        private Timer mTimer;

        public View onCreateView(@NonNull LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.fragment_slideshow, container, false);

            mViewPager = root.findViewById(R.id.viewpager);
            CircleIndicator circleIndicator = root.findViewById(R.id.circle_indicator);

            mSlideImages = getListSlideImage();
            SlideImageAdapter slideImageAdapter = new SlideImageAdapter(getActivity(), mSlideImages);
            mViewPager.setAdapter(slideImageAdapter);

            circleIndicator.setViewPager(mViewPager);
            slideImageAdapter.registerDataSetObserver(circleIndicator.getDataSetObserver());

            autoRunSlideImage();

            return root;
        }

        private List<SlideImage> getListSlideImage() {
            List<SlideImage> list = new ArrayList<>();
            list.add(new SlideImage(R.drawable.nooelmoon7a));
            list.add(new SlideImage(R.drawable.nooelmoon5a));
            list.add(new SlideImage(R.drawable.nooelmoon9a));
            list.add(new SlideImage(R.drawable.nooelmoon15a));
            list.add(new SlideImage(R.drawable.nooelmoon110a));

            return list;
        }

        private void autoRunSlideImage() {
            if (mSlideImages == null || mSlideImages.isEmpty() || mViewPager == null) {
                return;
            }

            if (mTimer == null) {
                mTimer = new Timer();
            }
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int currentItem = mViewPager.getCurrentItem();
                            int totalItem = mSlideImages.size() - 1;
                            if (currentItem < totalItem) {
                                currentItem++;
                                mViewPager.setCurrentItem(currentItem);
                            } else {
                                mViewPager.setCurrentItem(0);
                            }
                        }
                    });
                }
            }, 500, 3000);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            if (mTimer != null) {
                mTimer.cancel();
                mTimer = null;
            }
        }
    }