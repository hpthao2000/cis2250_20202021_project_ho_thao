package com.thao.flowershopthao.ui.splash;
//Easy Splash Screen - Android Studio Tutorial - https://www.youtube.com/watch?v=gt1WYT0Qpfk

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.thao.flowershopthao.MainActivity;
import com.thao.flowershopthao.R;

import gr.net.maroulis.library.EasySplashScreen;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EasySplashScreen config = new EasySplashScreen(SplashScreenActivity.this)
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(4000)
                .withBackgroundColor(Color.parseColor("#FFBB86FC"))
                .withHeaderText("Flowershop Application")
                .withFooterText("Project - CIS2250 2020~2021")
                .withBeforeLogoText("Instructor: BJ Maclean")
                .withAfterLogoText("Thao Ho")
                .withLogo(R.mipmap.ic_launcher_thao_round);

        config.getHeaderTextView().setTextColor(Color.WHITE);
        config.getFooterTextView().setTextColor(Color.WHITE);
        config.getBeforeLogoTextView().setTextColor(Color.WHITE);
        config.getAfterLogoTextView().setTextColor(Color.WHITE);

        View easySplashscreen = config.create();
        setContentView(easySplashscreen);
    }
}