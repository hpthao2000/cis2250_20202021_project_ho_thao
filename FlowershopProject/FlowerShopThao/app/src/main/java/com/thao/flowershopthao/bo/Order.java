package com.thao.flowershopthao.bo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


/**
 * This class will represent a camper.  It will be used for loading the data from the json string.
 * As well, it has the annotations needed to allow it to be used for the Room database.
 * @since 20200202
 * @author BJM
 */

@Entity(tableName = "flowerorder")
public class Order {

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "orderDate")
    private String orderDate;
    @ColumnInfo(name = "totaCost ")
    private String totaCost;
  /*   @ColumnInfo(name = "campType")
    private String dob;
      @ColumnInfo(name = "campType")
    private Integer campType;
    @ColumnInfo(name = "campTypeDescription")
    private String campTypeDescription;



    public String getCampTypeDescription() {
        return campTypeDescription;
    }

    public void setCampTypeDescription(String campTypeDescription) {
        this.campTypeDescription = campTypeDescription;
    }*/



    public Order() {
    }

    public Order(Integer id) {
        this.id = id;
    }

    public Order(Integer id, String orderDate, String totaCost) {
        this.id = id;
        this.orderDate = orderDate;
        this.totaCost = totaCost;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getTotaCost() {
        return totaCost;
    }

    public void setTotaCost(String totaCost) {
        this.totaCost = totaCost;
    }
/*
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getCampType() {
        return campType;
    }

    public void setCampType(Integer campType) {
        this.campType = campType;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof com.thao.flowershopthao.bo.Order)) {
            return false;
        }
        com.thao.flowershopthao.bo.Order other = (com.thao.flowershopthao.bo.Order) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "#"+id+" Date: "+this.orderDate+" Total: "+this.totaCost;
    }

}
