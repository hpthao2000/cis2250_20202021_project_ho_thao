package com.thao.flowershopthao.ui.customerdetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.thao.flowershopthao.R;
import com.thao.flowershopthao.bo.Customer;
import com.thao.flowershopthao.MainActivity;

public class DetailCustomerFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail_customer, container, false);

        TextView tvFullName = root.findViewById(R.id.tv_full_name);
        TextView tvDateOfBirth = root.findViewById(R.id.tv_date_of_birth);
        TextView tvCity = root.findViewById(R.id.tv_city);
        TextView tvAddress = root.findViewById(R.id.tv_address);
        Button btnShareContact = root.findViewById(R.id.btn_share_contact);

        Bundle bundle = getArguments();
        if (bundle != null) {
            Customer customer = (Customer) bundle.get("object_customer");
            tvFullName.setText(customer.getFullName());
            tvDateOfBirth.setText(customer.getBirthDate());
            tvCity.setText(customer.getCity());
            tvAddress.setText(customer.getAddress1());

            btnShareContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickShareContact(customer);
                }
            });
        }

        return root;
    }

    private void onClickShareContact(Customer customer) {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null) {
            return;
        }

        Intent emailIntent = new Intent(Intent.ACTION_SEND)
                .setType("plain/text")
                .putExtra(Intent.EXTRA_SUBJECT, "Flower Shop Application")
                .putExtra(Intent.EXTRA_TEXT, customer.getFullName()
                        + "\n" + customer.getBirthDate()
                        + "\n" + customer.getCity()
                        + "\n" + customer.getAddress1());
        mainActivity.startActivity(Intent.createChooser(emailIntent, "Send Email"));
    }
}