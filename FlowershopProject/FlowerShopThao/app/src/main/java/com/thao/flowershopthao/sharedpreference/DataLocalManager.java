package com.thao.flowershopthao.sharedpreference;

import android.content.Context;

public class DataLocalManager {

    private static final String PREF_LOAD_DATA_FROM_DATABASE = "PREF_LOAD_DATA_FROM_DATABASE";

    private static DataLocalManager instance;
    private MySharedPreference mySharedPreference;

    public static void init(Context context) {
        instance = new DataLocalManager();
        instance.mySharedPreference = new MySharedPreference(context);
    }

    public static DataLocalManager getInstance() {
        if (instance == null) {
            instance = new DataLocalManager();
        }
        return instance;
    }

    public static void setStatusLoadDataFromDatabase(boolean status) {
        DataLocalManager.getInstance().mySharedPreference.putBooleanValue(PREF_LOAD_DATA_FROM_DATABASE, status);
    }

    public static boolean getStatusLoadDataFromDatabase() {
        return DataLocalManager.getInstance().mySharedPreference.getBooleanValue(PREF_LOAD_DATA_FROM_DATABASE);
    }
}
