package com.thao.flowershopthao;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.thao.flowershopthao.bo.Customer;
import com.thao.flowershopthao.dao.MyAppDatabase;
import com.thao.flowershopthao.ui.broadcast.ConnectionStatus;
import com.thao.flowershopthao.ui.customers.CustomersFragment;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements CustomersFragment.OnListFragmentInteractionListener {

    private AppBarConfiguration mAppBarConfiguration;
    public static MyAppDatabase myAppDatabase;
    private ConnectionStatus connectionStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { goToAddCustomerFragment();}
        });
        connectionStatus = new ConnectionStatus();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,
                R.id.nav_add_customer,
                R.id.nav_customers, R.id.nav_list_customer,
                R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_settings, R.id.nav_help, R.id.nav_about,
                R.id.nav_our_location,
                R.id.nav_linear_layout)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        //BJM 20200131 Build the app database object
        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "customer.db").allowMainThreadQueries().build();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    /*
     * Method used when navigating from the menu items
     * https://developer.android.com/guide/navigation/navigation-ui
     *
     * Date: 2021/01/22
     * Purpose: used for material design presentation - feedback fragment
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.onNavDestinationSelected(item, navController)
                || super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    /**
     * This method will be used in the customer fragment when the user clicks on a row of the
     * customer recyclerview.  This method will transfer the user to a details fragment.  The
     * id of the camper will be passed to the fragment and used to load the correct customer details.
     * Will use the array list associated with the recyclerview.
     *
     * @param item the customer
     * @author BJM
     * @since 20200124
     */
    @Override
    public void onListFragmentInteraction(Customer item) {
        /*
            BJM 20200131
            Send the user to a details fragment.
        */
        Log.d("bjm", "item communicated from fragment: " + item.toString());
        /* BJM 20200202
           Put the json representation of the customer into the bundle to be passed to the fragment.
           This will be used in the details fragment.
        */
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        String jsonCustomer = gson.toJson(item);
        bundle.putString("customer", jsonCustomer);
        /*
          BJM 20200202
          Use the navigation controller object stored as an attribute of the main
          activity to nagivate the ui to the customer detail fragment.
        */
        Log.d("BJM","Changing nav_host_fragment to the customer detail fragment"+jsonCustomer);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.navigate(R.id.nav_customer_detail, bundle);
    }

    public void goToOurLocationFragment() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.navigate(R.id.nav_our_location);
    }

    private void goToAddCustomerFragment() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.navigate(R.id.nav_add_customer);
    }

    public void goToCustomerDetailsFragment(Customer customer) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("object_customer", (Serializable) customer);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.navigate(R.id.nav_customer_details, bundle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectionStatus, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(connectionStatus);
    }

}




