package com.thao.flowershopthao.ui.about;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.thao.flowershopthao.R;
import com.thao.flowershopthao.MainActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class OurLocationFragment extends Fragment implements OnMapReadyCallback {

    private static final double LATITUDE = 	46.238888;
    private static final double LONGITUDE = -63.129166;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_our_location, container, false);

        // init map
        SupportMapFragment supportMapFragment = ((SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.fragment_view_map));
        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(this);
        }
        return root;
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LatLng currentLocation = new LatLng(LATITUDE, LONGITUDE);
        googleMap.addMarker(new MarkerOptions()
                .position(currentLocation)).setTitle("Our Location");
        CameraUpdate myLoc = CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(currentLocation).zoom(13).build());
        googleMap.moveCamera(myLoc);
    }
}