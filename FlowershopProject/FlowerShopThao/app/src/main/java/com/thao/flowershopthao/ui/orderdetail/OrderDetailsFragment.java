package com.thao.flowershopthao.ui.orderdetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import com.thao.flowershopthao.R;
import com.thao.flowershopthao.bo.Order;


public class OrderDetailsFragment extends Fragment {

    private Order order;
    private TextView tvID;
    private TextView tvOD;
    private TextView tvTotal;
    private ImageButton buttonAddContact;
    private ImageButton buttonSendEmail;


    public static OrderDetailsFragment newInstance() {
        return new OrderDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
          BJM 20200202
          Note how the arguments are passed from the activity to this fragment.  The
          camper object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if (getArguments() != null) {
            String orderJson = getArguments().getString("order");
            Gson gson = new Gson();
            order = gson.fromJson(orderJson, Order.class);
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.order_details_fragment, container, false);

        tvID = view.findViewById(R.id.textViewDetailID);
        tvOD = view.findViewById(R.id.textViewDetailOD);
        tvTotal= view.findViewById(R.id.textViewDetailTotal);

        tvID.setText(order.getId());

        tvOD.setText(order.getTotaCost());
        tvTotal.setText(order.getTotaCost());

        return view;
    }

}
