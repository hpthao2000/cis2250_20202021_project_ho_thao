package com.thao.flowershopthao.ui.customers;

import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.thao.flowershopthao.R;
import com.thao.flowershopthao.bo.Customer;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Camper} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyCustomerListRecyclerViewAdapter extends RecyclerView.Adapter<MyCustomerListRecyclerViewAdapter.ViewHolder> {

    private final List<Customer> mValues;
    private final CustomersFragment.OnListFragmentInteractionListener mListener;

    public MyCustomerListRecyclerViewAdapter(List<Customer> items, CustomersFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_customer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
//        holder.mIdView.setText(mValues.get(position).getId().toString());

        holder.mFullNameView.setText(mValues.get(position).getFullName());
        holder.mAddress1View.setText(mValues.get(position).getAddress1());

        /* BJM 20200202
           When the user selects a row from the recyclerview, this on click listener will handle
           the event.  The onListFragmentInteraction method that is implemented in the MainActivity
           will be called.  This method is used to allow this fragment to communicate with the
           activity.  The activity will then take care of sending the ui to the detail fragment
           associated with the row that was clicked in the recyclerview.
         */

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("BJM", "clicked on a row of the recyclerview.  ");


                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        //        public final TextView mIdView;
        public final TextView mFullNameView;
        public final TextView mAddress1View;
        public Customer mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
//            mIdView = (TextView) view.findViewById(R.id.item_number);
            mFullNameView = (TextView) view.findViewById(R.id.full_name);
           mAddress1View = (TextView) view.findViewById(R.id.address1);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mFullNameView.toString() + "'" + mAddress1View.toString() + "'";
        }
    }


    }

