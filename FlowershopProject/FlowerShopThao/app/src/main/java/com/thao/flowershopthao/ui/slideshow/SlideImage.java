package com.thao.flowershopthao.ui.slideshow;

public class SlideImage {

    private int resourceId;

    public SlideImage(int resourceId) {
        this.resourceId = resourceId;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }
}
