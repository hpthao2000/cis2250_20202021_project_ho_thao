package com.thao.flowershopthao.dao;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.thao.flowershopthao.R;
import com.thao.flowershopthao.bo.Customer;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter< com.thao.flowershopthao.dao.CustomerAdapter.CustomerViewHolder> {

    private List<Customer> mListCustomer;
    private IClickDetailListener iClickDetailListener;

    public interface IClickDetailListener {
        void onClickCustomerDetails(Customer customer);
    }

    public CustomerAdapter(List<Customer> mListCustomer, IClickDetailListener listener) {
        this.mListCustomer = mListCustomer;
        this.iClickDetailListener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer, parent, false);
        return new CustomerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerViewHolder holder, int position) {
        Customer customer = mListCustomer.get(position);
        if (customer == null) {
            return;
        }

        holder.tvFullName.setText(customer.getFullName());
        holder.tvAddress.setText(customer.getAddress1());

        holder.layoutItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iClickDetailListener.onClickCustomerDetails(customer);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListCustomer != null) {
            return mListCustomer.size();
        }
        return 0;
    }

    public class CustomerViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout layoutItem;
        private TextView tvFullName;
        private TextView tvAddress;

        public CustomerViewHolder(@NonNull View itemView) {
            super(itemView);

            layoutItem = itemView.findViewById(R.id.layout_item);
            tvFullName = itemView.findViewById(R.id.tv_full_name);
            tvAddress = itemView.findViewById(R.id.tv_address);
        }
    }
}
