package com.thao.flowershopthao.ui.slideshow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.thao.flowershopthao.R;

import java.util.List;

public class SlideImageAdapter extends PagerAdapter {

    private Context mContext;
    private List<SlideImage> mSlideImages;

    public SlideImageAdapter(Context mContext, List<SlideImage> mSlideImages) {
        this.mContext = mContext;
        this.mSlideImages = mSlideImages;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_slide_image, container, false);
        ImageView imgSlide = view.findViewById(R.id.img_slide);

        SlideImage slideImage = mSlideImages.get(position);
        if (slideImage != null) {
            Glide.with(mContext).load(slideImage.getResourceId()).into(imgSlide);
        }
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        if (mSlideImages != null) {
            return mSlideImages.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
