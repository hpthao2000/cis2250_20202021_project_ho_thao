package com.thao.flowershopthao.api;

import com.thao.flowershopthao.bo.Customer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public interface ApiService {

    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    com.thao.flowershopthao.api.ApiService apiService = new Retrofit.Builder()
            .baseUrl(Const.CUSTOMER_BASE_API)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(com.thao.flowershopthao.api.ApiService.class);

    @GET("customers")
    Call<List<Customer>> getListCustomers();
}
