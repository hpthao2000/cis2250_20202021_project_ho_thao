package com.thao.flowershopthao.ui.customerdetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import com.thao.flowershopthao.MainActivity;
import com.thao.flowershopthao.R;
import com.thao.flowershopthao.bo.Customer;


public class CustomerDetailsFragment extends Fragment {

    private Customer customer;
    private TextView tvFullName;
    private TextView tvAddress1;
    private TextView tvDOB;



    public static CustomerDetailsFragment newInstance() {
        return new CustomerDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
          BJM 20200202
          Note how the arguments are passed from the activity to this fragment.  The
          camper object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if (getArguments() != null) {
            String customerJson = getArguments().getString("customer");
            Gson gson = new Gson();
            customer = gson.fromJson(customerJson, Customer.class);
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.customer_details_fragment, container, false);

        tvFullName = view.findViewById(R.id.textViewDetailFullName);
        tvAddress1 = view.findViewById(R.id.textViewDetailAddress1);
        tvDOB = view.findViewById(R.id.textViewDetailBirthDate);

        tvFullName.setText(customer.getFullName());
        tvAddress1.setText(customer.getAddress1());
        tvDOB.setText(customer.getBirthDate());

        return view;
    }
}




