package com.thao.flowershopthao.util;

import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.thao.flowershopthao.R;

/**
 * Notification Util class will provide easy access to send a notification to the user.  Note that the
 * Note that the NotificationApplication will need to have the context set for this to work.
 * @since 20210329
 * @author BJM
 */

public class NotificationUtil {

    public static synchronized void sendNotification(String title, String message) {
        //Channel Id is ignored on lower APIs
        Log.d("bjm notification", "Sending a notification");
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(com.thao.flowershopthao.util.NotificationApplication.getContext(), com.thao.flowershopthao.util.NotificationApplication.MEMBER_CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_menu_send)
                        .setContentTitle(title)
                        .setContentText(message);

        NotificationManager notificationManager = (NotificationManager) com.thao.flowershopthao.util.NotificationApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }
}
