package com.thao.flowershopthao.api;

import java.util.List;

import com.thao.flowershopthao.bo.Customer;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonCustomerApi {

    /**
     * This abstract method to be created to allow retrofit to get list of customers
     * @return List of customers
     * @since 202210224
     * @author thao (with help from the retrofit research.
     */

    @GET("customers")
    Call<List<Customer>> getCustomers();

}
