package com.thao.flowershopthao.ui.orders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.thao.flowershopthao.R;
import com.thao.flowershopthao.bo.Order;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Order} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyOrderListRecyclerViewAdapter extends RecyclerView.Adapter<MyOrderListRecyclerViewAdapter.ViewHolder> {

    private final List<Order> mValues;
    private final OrdersFragment.OnListFragmentInteractionListener mListener;

    public MyOrderListRecyclerViewAdapter(List<Order> items, OrdersFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
//        holder.mIdView.setText(mValues.get(position).getId().toString());
        holder.mOrderDateView.setText(mValues.get(position).getOrderDate());
        holder.mTotalView.setText(mValues.get(position).getTotaCost());

        /* BJM 20200202
           When the user selects a row from the recyclerview, this on click listener will handle
           the event.  The onListFragmentInteraction method that is implemented in the MainActivity
           will be called.  This method is used to allow this fragment to communicate with the
           activity.  The activity will then take care of sending the ui to the detail fragment
           associated with the row that was clicked in the recyclerview.
         */

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("BJM", "clicked on a row of the recyclerview.  ");


                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
//        public final TextView mIdView;
        public final TextView mOrderDateView;
        public final TextView mTotalView;
        public Order mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
//            mIdView = (TextView) view.findViewById(R.id.item_number);
            mOrderDateView = (TextView) view.findViewById(R.id.order_date);
            mTotalView = (TextView) view.findViewById(R.id.total);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTotalView.toString() + "'";
        }
    }
}
