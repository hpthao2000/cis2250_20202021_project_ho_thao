package com.thao.flowershopthao.bo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


/**
 * This class will represent a camper.  It will be used for loading the data from the json string.
 * As well, it has the annotations needed to allow it to be used for the Room database.
 * @since 20200202
 * @author BJM
 */

@Entity(tableName = "customer")
public class Customer {

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @ColumnInfo(name = "fullName")
    private String fullName;
    @ColumnInfo(name = "address1")
    private String address1;
    @ColumnInfo(name = "city")
    private String city;
    @ColumnInfo(name = "province")
    private String province;
    @ColumnInfo(name = "birthDate")
    private String birthDate;




    public Customer() {
    }

    public Customer(Integer id) {
        this.id = id;
    }

    public Customer(Integer id, String fullName, String birthDate) {
        this.id = id;
        this.fullName = fullName;
        this.address1 = address1;
        this.city = city;
        this.province = province;
        this.birthDate = birthDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String dob) {
        this.birthDate = birthDate;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof com.thao.flowershopthao.bo.Customer)) {
            return false;
        }
        com.thao.flowershopthao.bo.Customer other = (com.thao.flowershopthao.bo.Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "#"+id+" Name: "+this.fullName+"  "+this.address1+" "+this.birthDate;
    }

}
