package com.thao.flowershopthao.bo;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.thao.flowershopthao.api.JsonCustomerApi;
import com.thao.flowershopthao.ui.customers.CustomersFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class CustomerContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<com.thao.flowershopthao.bo.Customer> CUSTOMERS = new ArrayList<com.thao.flowershopthao.bo.Customer>();


    /**
     * Load the campers.  This method will use the rest service to provide the data.  The reason it is
     * in this class is because it is changing the value to the CAMPERS list.  This is the list which
     * is used to back the RecyclerView.
     *
     * @author BJM taken from Alex/Thomas' presentation.
     * @since 20200116
     */

    public static void loadCustomers(Activity context) {

        Log.d("BJM", "Accessing api at:" +com.thao.flowershopthao.bo.Util.CUSTOMER_BASE_API);

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(com.thao.flowershopthao.bo.Util.CUSTOMER_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonCustomerApi jsonCustomerApi = retrofit.create(JsonCustomerApi.class);

        //Create a list of orders.
        Call<List<com.thao.flowershopthao.bo.Customer>> call = jsonCustomerApi.getCustomers();

        //final reference to activity to get shared preferences
        final Activity contextIn = context;

        call.enqueue(new Callback<List<com.thao.flowershopthao.bo.Customer>>() {

            @Override
            public void onResponse(Call<List<com.thao.flowershopthao.bo.Customer>> call, Response<List<com.thao.flowershopthao.bo.Customer>> response) {

                List<com.thao.flowershopthao.bo.Customer> customers;

                customers = response.body();

                Log.d("bjm", "data back from service call #returned=" + customers.size());

                //**********************************************************************************
                // Now that we have the campers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                com.thao.flowershopthao.bo.CustomerContent.CUSTOMERS.clear();
                com.thao.flowershopthao.bo.CustomerContent.CUSTOMERS.addAll(customers);

                //**********************************************************************************
                // The CamperListFragment has a recyclerview which is used to show the camper list.
                // This recyclerview is backed by the camper list in the CamperContent class.  After
                // this list is loaded, need to notify the adapter from the recyclerview that the
                // data is changed.
                //**********************************************************************************

                CustomersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();

                //Remove the progress bar from the view.
                CustomersFragment.clearProgressBarVisitiblity();

            }

            @Override
            public void onFailure(Call<List<com.thao.flowershopthao.bo.Customer>> call, Throwable t) {

                //**********************************************************************************
                // If the api call failed, give a notification to the user.
                //**********************************************************************************
                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());

            }
        });
    }
}