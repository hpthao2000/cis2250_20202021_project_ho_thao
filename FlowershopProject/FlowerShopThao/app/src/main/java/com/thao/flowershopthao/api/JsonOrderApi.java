package com.thao.flowershopthao.api;

import java.util.List;

import com.thao.flowershopthao.bo.Order;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonOrderApi {

    /**
     * This abstract method to be created to allow retrofit to get list of orders
     * @return List of orders
     * @since 202210224
     * @author thao (with help from the retrofit research.
     */

    @GET("orders")
    Call<List<Order>> getOrders();

}
