package com.thao.flowershopthao.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import com.thao.flowershopthao.bo.Order;


@Dao
public interface OrderDAO {

    @Insert
    public long add(Order order);

    @Update
    public void update(Order order);
    @Delete
    public void delete(Order order);

    @Query("select * from flowerorder")
    public List<Order> get();

    @Query("select * from flowerorder where id =:id")
    public Order get(int id);

    @Query("delete from flowerorder")
    public void deleteAll();

}
