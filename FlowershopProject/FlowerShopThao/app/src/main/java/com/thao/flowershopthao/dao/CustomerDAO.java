package com.thao.flowershopthao.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import com.thao.flowershopthao.bo.Customer;
import com.thao.flowershopthao.bo.Order;


@Dao
public interface CustomerDAO {

    @Insert
    public long add(Customer customer);

    @Update
    public void update(Customer customer);
    @Delete
    public void delete(Customer customer);

    @Query("select * from customer")
    public List<Customer> get();

    @Query("select * from customer where id =:id")
    public Order get(int id);

    @Query("delete from customer")
    public void deleteAll();

    @Insert
    void insertCustomer(Customer customer);

    @Query("SELECT * FROM customer")
    List<Customer> getListCustomerFromDatabase();

    @Query("SELECT * FROM customer WHERE id = :id")
    List<Customer> checkExistCustomer(int id);

}
