package com.thao.flowershopthao.ui.about;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.thao.flowershopthao.MainActivity;
import com.thao.flowershopthao.R;

import java.util.List;

public class AboutFragment extends Fragment {



        private static final String LINK_FACEBOOK = "https://www.facebook.com/Hpthao2000";
        private static final String FACEBOOK_ID = "100000117325987";

        public View onCreateView(@NonNull LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.fragment_about, container, false);

            Button btnFacebookPage = root.findViewById(R.id.btn_facebook_page);
            btnFacebookPage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickOpenFacebook(getActivity());
                }
            });

            Button btnLocation = root.findViewById(R.id.btn_location);
            btnLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickOpenLocation();
                }
            });

            return root;
        }

        private void onClickOpenFacebook(Context context) {
            Intent intent;
            try {
                context.getPackageManager()
                        .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/" + FACEBOOK_ID));
            } catch (Exception e) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(LINK_FACEBOOK)); //catches and opens a url to the desired page
            }
            context.startActivity(intent);
        }

        private void onClickOpenLocation() {
            MainActivity mainActivity = (MainActivity) getActivity();
            if (mainActivity == null) {
                return;
            }

            PermissionListener permissionlistener = new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    mainActivity.goToOurLocationFragment();
                }

                @Override
                public void onPermissionDenied(List<String> deniedPermissions) {
                    Toast.makeText(mainActivity, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                }
            };

            TedPermission.with(mainActivity)
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                    .check();
        }
    }