package com.thao.flowershopthao.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.thao.flowershopthao.bo.Customer;


@Database(entities = {Customer.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract com.thao.flowershopthao.dao.CustomerDAO customerDAO();

}


/*

@Database(entities = {Order.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract com.thao.flowershopthao.dao.OrderDAO orderDAO();

}
*/
