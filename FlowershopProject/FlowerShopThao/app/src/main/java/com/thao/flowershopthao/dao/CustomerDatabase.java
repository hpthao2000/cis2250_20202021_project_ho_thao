package com.thao.flowershopthao.dao;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.thao.flowershopthao.bo.Customer;

@Database(entities = {Customer.class}, version = 1)
public abstract class CustomerDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "customer.db";
    private static com.thao.flowershopthao.dao.CustomerDatabase instance;

    public static synchronized com.thao.flowershopthao.dao.CustomerDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), com.thao.flowershopthao.dao.CustomerDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract CustomerDAO customerDAO();
}
