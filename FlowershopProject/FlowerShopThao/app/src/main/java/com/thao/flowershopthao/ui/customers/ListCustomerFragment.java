package com.thao.flowershopthao.ui.customers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thao.flowershopthao.R;
import com.thao.flowershopthao.dao.CustomerAdapter;
import com.thao.flowershopthao.api.ApiService;
import com.thao.flowershopthao.dao.CustomerDatabase;
import com.thao.flowershopthao.bo.Customer;
import com.thao.flowershopthao.sharedpreference.DataLocalManager;
import com.thao.flowershopthao.MainActivity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.thao.flowershopthao.MyApplication.CHANNEL_ID;

public class ListCustomerFragment extends Fragment {

    private RecyclerView rcvCustomer;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_customer, container, false);
        rcvCustomer = root.findViewById(R.id.rcv_customer);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcvCustomer.setLayoutManager(linearLayoutManager);

        callApiGetListCustomer();
        return root;
    }

    private void callApiGetListCustomer() {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity == null) {
            return;
        }

        if (DataLocalManager.getStatusLoadDataFromDatabase()) {
            List<Customer> customers = CustomerDatabase.getInstance(getActivity()).customerDAO().getListCustomerFromDatabase();
            if (customers != null && !customers.isEmpty()) {
                CustomerAdapter customerAdapter = new CustomerAdapter(customers, new CustomerAdapter.IClickDetailListener() {
                    @Override
                    public void onClickCustomerDetails(Customer customer) {
                        mainActivity.goToCustomerDetailsFragment(customer);
                    }
                });
                rcvCustomer.setAdapter(customerAdapter);
                sendPushNotification(mainActivity, customers, "Load (" + customers.size() + ") customers from Room Database");
            }
        } else {
            ApiService.apiService.getListCustomers().enqueue(new Callback<List<Customer>>() {
                @Override
                public void onResponse(@NotNull Call<List<Customer>> call, @NotNull Response<List<Customer>> response) {
                    List<Customer> customers = response.body();
                    if (customers != null && !customers.isEmpty()) {
                        Log.e("ListCustomerFragment", customers.size() + "");

                        // Add customer to database
                        for (int i = 0; i < customers.size(); i++) {
                            Customer customerEntity = customers.get(i);
                            if (!isCustomerExist(customerEntity)) {
                                CustomerDatabase.getInstance(getActivity()).customerDAO().insertCustomer(customerEntity);
                            }
                        }

                        CustomerAdapter customerAdapter = new CustomerAdapter(customers, new CustomerAdapter.IClickDetailListener() {
                            @Override
                            public void onClickCustomerDetails(Customer customer) {
                                mainActivity.goToCustomerDetailsFragment(customer);
                            }
                        });
                        rcvCustomer.setAdapter(customerAdapter);
                        sendPushNotification(mainActivity, customers, "Load (" + customers.size() + ") customers from API");
                    }
                }

                @Override
                public void onFailure(@NotNull Call<List<Customer>> call, @NotNull Throwable t) {
                    Log.e("ListCustomerFragment", "Call api get list customer fail");
                }
            });
        }
    }


    private boolean isCustomerExist(Customer customer) {
        List<Customer> list = CustomerDatabase.getInstance(getActivity()).customerDAO().checkExistCustomer(customer.getId());
        return list != null && !list.isEmpty();
    }

    private void sendPushNotification(Context context, List<Customer> customers, String message) {
        if (context == null || customers == null || customers.isEmpty()) {
            return;
        }

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher_flower)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(1, notification);
        }
    }
}