package com.thao.flowershopthao.ui.setting;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.thao.flowershopthao.R;
import com.thao.flowershopthao.sharedpreference.DataLocalManager;

public class SettingsFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        Switch swich = root.findViewById(R.id.switch_on_off);
        swich.setChecked(DataLocalManager.getStatusLoadDataFromDatabase());

        swich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DataLocalManager.setStatusLoadDataFromDatabase(isChecked);
            }
        });
        return root;
    }
}